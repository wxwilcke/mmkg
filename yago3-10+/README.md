# YAGO3-10+

General knowledge about people, countries, cities, movies, and organisations.

`yago3-10+_train.nt.gz`
- training split; RDF version with scaled-down images

`yago3-10+_test.nt.gz`
- test split; RDF version with scaled-down images

`yago3-10+_valid.nt.gz`
- validation split; RDF version with scaled-down images

`scripts/`
- scripts to convert YAGO3-10+ to RDF

## Statistics

| Stats     | Count     |
|-----------|-----------|
| Facts     | 1,368,936 |
| Relations | 44        |
| Entities  | 135,653   |
| Literals  | 279,902   |

| Modality  | Count   |
|-----------|---------|
| Numerical | 0       |
| Temporal  | 111,406 |
| Textual   | 107,319 |
| Visual    | 61,177  |
| Spatial   | 0       |
| Other     | 0       |

## References

Original dataset from

`Pezeshkpour, Pouya, Liyan Chen, and Sameer Singh. ‘Embedding Multimodal Relational Data for Knowledge Base Completion’. ArXiv:1809.01341 [Cs, Stat], 7 September 2018. http://arxiv.org/abs/1809.01341`
