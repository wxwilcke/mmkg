#!/usr/bin/env python

import gzip
from io import BytesIO
import tarfile

from base64 import urlsafe_b64encode
from PIL import Image
from urllib import parse


NAMESPACE = "https://yago-knowledge.org/"
_IMG_SIZE = (64, 64)
_IMG_MODE = "RGB"

def ntriple(s, p, o, object_type="uri", namespace=(None, None, None)):
    t = ""
    t += '<'+NAMESPACE+'resource/'+s.title()+'> ' if namespace[0] is None else\
         '<'+namespace[0]+s.title()+'> '
    t += '<'+NAMESPACE+'schema/'+p+'> ' if namespace[1] is None else\
         '<'+namespace[1]+p+'> '

    if object_type == "uri":
        t += '<'+NAMESPACE+'resource/'+o+'> ' if namespace[2] is None else\
             '<'+namespace[2]+o+'> '
    else:
        t += "\"%s\"^^<http://www.w3.org/2001/XMLSchema#%s> " % (o, object_type)

    return t + '.'


def mkbinary(im):
    b = BytesIO()
    im.save(b, format="JPEG")

    return urlsafe_b64encode(b.getvalue()).decode()

def convert_mode(im):
    if im.mode != _IMG_MODE:
        im = im.convert(_IMG_MODE)

    return im

def downsample(im):
    if im.size != _IMG_SIZE:
        return im.resize(_IMG_SIZE)

    return im

def main():
    with tarfile.open("YAGO3-10.tar.gz", "r:gz") as tf:
        for filename in ("train", "valid", "test"):
            f_in = tf.extractfile(filename+".txt")

            with gzip.open(filename+".nt.gz", 'wb') as f_out:
                for line in f_in:
                    s, p, o = line.decode().rstrip().split('\t')
                    t = ntriple(parse.quote(s),
                                parse.quote(p),
                                parse.quote(o)) + '\n'
                    f_out.write(t.encode())

    for filename in ("Numerical data_train", "Numerical  data_test"):
        with open(filename+".txt", 'r') as f_in:
            filename = filename.lower().replace("  ", " ").replace(" ", "_")
            with gzip.open(filename+".nt.gz", 'wb') as f_out:
                for line in f_in:
                    s, p, o = line.rstrip().split('\t')
                    t = ntriple(parse.quote(s),
                                parse.quote(p),
                                parse.quote(o),
                                object_type="gYear") + '\n'
                    f_out.write(t.encode())

    with open("Textual data.txt", 'r') as f_in:
        with gzip.open("textual_data.nt.gz", 'wb') as f_out:
            p = "description"
            for line in f_in:
                line = line.rstrip()
                s, o = line.split('\t')
                s = parse.quote(s)
                o = parse.quote(o)
                t = ntriple(s, p, o, object_type="string",
                            namespace=(None, 'http://purl.org/dc/terms/', None)) + '\n'
                f_out.write(t.encode())

    with tarfile.open("yago-10_images.tar.xz", "r:xz") as tf:
        with gzip.open("b64images.nt.gz", 'wb') as f_out:
            p = "depiction"
            for filename in tf.getmembers():
                if filename.name[-3:] != "jpg":
                    continue

                o = tf.extractfile(filename)
                try:
                    im = Image.open(o)
                    im = convert_mode(im)
                    im = downsample(im)
                except:
                    continue

                s = parse.quote(filename.name[7:-4])
                t = ntriple(s, p, mkbinary(im), object_type="b64string",
                            namespace=(None, 'http://xmlns.com/foaf/spec/', None)) + '\n'
                f_out.write(t.encode())

if __name__ == "__main__":
    main()
