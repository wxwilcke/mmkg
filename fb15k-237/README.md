# FB15k-237

General knowledge about people, countries, and more.

`freebase_mtr100_mte100_237-train.nt.gz`
- training split; RDF version of original 

`freebase_mtr100_mte100_237-test.nt.gz`
- test split; RDF version of original 

`freebase_mtr100_mte100_237-valid.nt.gz`
- validation split; RDF version of original 

`scripts/`
- scripts to convert FB15k-237 to RDF

## Statistics

| Stats     | Count   |
|-----------|---------|
| Facts     | 310,116 |
| Relations | 237     |
| Entities  | 14,541  |
| Literals  | 0       |

| Modality  | Count |
|-----------|-------|
| Numerical | 0     |
| Temporal  | 0     |
| Textual   | 0     |
| Visual    | 0     |
| Spatial   | 0     |
| Other     | 0     |
|           |       |

## Source

Original dataset from:

Toutanova, Kristina, et al. "Representing text for joint embedding of text and knowledge bases." Proceedings of the 2015 conference on empirical methods in natural language processing. 2015.
