#!/usr/bin/env python

import gzip
import tarfile


NAMESPACE = "http://freebase.com"

def ntriple(s, p, o):
    t = ""
    for e in (s, p, o):
        t += '<' + NAMESPACE + e + '> '

    return t + ".\n"

def main():
    with tarfile.open("fb15k237.tgz", "r:gz") as tf:
        for slce in ("train", "valid", "test"):
            filename = "freebase_mtr100_mte100_237-" + slce
            f_in = tf.extractfile("FB15k237/" + filename+".txt")

            with gzip.open(filename+".nt.gz", "wb") as f_out:
                for line in f_in:
                    line = line.decode().rstrip()

                    t = ntriple(*line.split('\t'))
                    f_out.write(t.encode())

if __name__ == "__main__":
    main()
