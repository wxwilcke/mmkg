#!/usr/bin/env python

import gzip
import hashlib
from math import ceil

from rdflib import Graph,URIRef


CLASS_PRED = URIRef('https://data.labs.pdok.nl/rce/def/monumentCode')
HASH_MAP = {
    'monuments-803k.nt.gz': '06685f241adfb25cab6986f162c4d675',
    'geometries-803k.nt.gz': '4509fda57a0e0d0d79cb1ffa44c01c58',
    'images-803k.nt.gz': '72b8399002545500a927f5fa5b2e4ab1',
    'imagesB64Thumbs-803k.nt.gz': 'b3525f604551951ae99ebd0f0bf4951f',
    'municipalities-803k.nt.gz': '210f6d2420821dd011b7ff101ae38f51',
    'monumentsToGeometries-803k.nt.gz': 'c4c5b8dbf7bc9c8fb337d0b10f70dc06',
    'monumentsToImages-803k.nt.gz': 'acb354aa42243943c130462e3e1b1302',
    'monumentsToMunicipalities-803k.nt.gz': 'd8ec1e0c7bd4898ae629d505f85455cb'}

def validate_files(path):
    for filename, md5hash in HASH_MAP.items():
        with open(path+filename, 'rb') as f:
            if hashlib.md5(f.read()).hexdigest() != md5hash:
                raise Exception("Unexpected hash on file %s" % path+filename)

    return True

def load_graphs(path):
    source_graphs = dict()
    for filename in HASH_MAP.keys():
        h = Graph()
        with gzip.open(path+filename, 'rb') as gf:
            h.parse(gf, format='nt')

        source_graphs[filename.split('.')[0]] = h

    return source_graphs

def divide_members(members, train_test_ratio, train_valid_ratio):
    num_members = len(members)

    test_portion = ceil(num_members * train_test_ratio[1])
    remain = num_members - test_portion
    assert remain == int(num_members * train_test_ratio[0])

    valid_portion = ceil(remain * train_valid_ratio[1])
    train_portion = remain - valid_portion
    assert train_portion == int(remain * train_valid_ratio[0])

    return (members[:train_portion],
            members[train_portion:train_portion+test_portion],
            members[-valid_portion:])

def create_splits(g, train_test_ratio, train_valid_ratio, stratified):
    samples_map = dict()
    for s,p,o in g.triples((None, CLASS_PRED, None)):
        if o not in samples_map.keys():
            samples_map[o] = list()
        samples_map[o].append((s,p,o))

    for sample_class, members in samples_map.items():
        samples_map[sample_class] = sorted(members)  # ensure reproducability

    train_set, test_set, valid_set = Graph(), Graph(), Graph()
    if not stratified:
        samples = list()
        for members in samples_map.values():
            samples.extend(members)

        train, test, valid = divide_members(members,
                                            train_test_ratio,
                                            train_valid_ratio)

        for sample in train:
            train_set.add(sample)
        for sample in test:
            test_set.add(sample)
        for sample in valid:
            valid_set.add(sample)
    else:
        for members in samples_map.values():
            train, test, valid = divide_members(members,
                                                train_test_ratio,
                                                train_valid_ratio)

            for sample in train:
                train_set.add(sample)
            for sample in test:
                test_set.add(sample)
            for sample in valid:
                valid_set.add(sample)

    return (train_set, test_set, valid_set)

def main(file_path='./', train_test_ratio=(.9, .1), train_valid_ratio=(.9, .1),
         stratified=True):
    print("Checking files...", end=' ', flush=True)
    if validate_files(file_path):
        print("OK")

    print("Importing monument graph...", end=' ', flush=True)
    g = Graph()
    with gzip.open(file_path+'monuments-803k.nt.gz', 'rb') as f:
        g.parse(data=f.read(), format='nt')
    print("OK")

    print("Generating splits...", end=' ', flush=True)
    train_set, test_set, valid_set = create_splits(g, train_test_ratio,
                                                   train_valid_ratio,
                                                   stratified)
    print("OK")

    print("Filter monument graph...", end=' ', flush=True)
    for split_graph in (train_set, test_set, valid_set):
        for fact in split_graph.triples((None, None, None)):
            g.remove(fact)  # filter targets from context
    print("OK")

    print("Merging context graphs...", end=' ', flush=True)
    for filename in HASH_MAP.keys():
        with gzip.open(file_path+filename, 'rb') as f:
            g.parse(data=f.read(), format='nt')
    print("OK")

    return (g, train_set, test_set, valid_set)

if __name__ == "__main__":
    context, train_set, test_set, valid_set = main('./')

    print("Split Distribution:")
    for name, graph in zip(('train', 'test', 'valid'),
                           (train_set, test_set, valid_set)):
        print(" - %s : %d" % (name, len(graph)))

    print("Writing splits to disk...", end=' ', flush=True)
    for filename, graph in zip(('dmg-803k_train_set.nt.gz',
                                'dmg-803k_test_set.nt.gz',
                                'dmg-803k_valid_set.nt.gz',
                                'dmg-803k_context.nt.gz'),
                               (train_set, test_set, valid_set, context)):
        with gzip.open('./'+filename, 'wb') as f:
            f.write(graph.serialize(format='nt'))
    print("OK")
