# Movielens 100k+

Dataset about movies, users, and ratings given to these movies by the users.

`ml100k+-train.nt.gz`
- training split; RDF version with additional nodes for ratings and scaled-down images

`ml100k+-test.nt.gz`
- test split; RDF version with additional nodes for ratings and scaled-down images

`mk100k+-valid.nt.gz`
- validation split; RDF version with additional nodes for ratings and scaled-down images

`scripts/`
- scripts to convert Movielens 100k+ to RDF

## Statistics

| Stats     | Count   |
|-----------|---------|
| Facts     | 413,358 |
| Relations | 13      |
| Entities  | 102,667 |
| Literals  | 208,579 |

| Modality  | Count   |
|-----------|---------|
| Numerical | 101,886 |
| Temporal  | 101,681 |
| Textual   | 3,379   |
| Visual    | 1,651   |
| Spatial   | 0       |
| Other     | 0       |

## References

Original dataset from

`Pezeshkpour, Pouya, Liyan Chen, and Sameer Singh. ‘Embedding Multimodal Relational Data for Knowledge Base Completion’. ArXiv:1809.01341 [Cs, Stat], 7 September 2018. http://arxiv.org/abs/1809.01341`
