#!/usr/bin/env python

import gzip
import random

from rdflib import Graph


def main(train, test, valid, user, item, images):
    n = len(train) + len(test) + len(valid)
    test_ratio = len(test)/n
    valid_ratio = len(valid)/n

    for g in (user, item, images):
        # keep same ratio
        for t in g.triples((None, None, None)):
            p = random.random()
            if p <= test_ratio:
                test.add(t)
                continue

            if p <= (valid_ratio+test_ratio):
                valid.add(t)
                continue

            train.add(t)

    return (train, test, valid)

if __name__ == "__main__":
    train = Graph()
    with gzip.open('../u.data.train.nt.gz', 'rb') as gf:
        train.parse(data=gf.read(), format='nt')

    test = Graph()
    with gzip.open('../u.data.test.nt.gz', 'rb') as gf:
        test.parse(data=gf.read(), format='nt')

    valid = Graph()
    with gzip.open('../u.data.valid.nt.gz', 'rb') as gf:
        valid.parse(data=gf.read(), format='nt')

    user = Graph()
    with gzip.open('../u.user.nt.gz', 'rb') as gf:
        user.parse(data=gf.read(), format='nt')

    item = Graph()
    with gzip.open('../u.item.nt.gz', 'rb') as gf:
        item.parse(data=gf.read(), format='nt')

    images = Graph()
    with gzip.open('../b64images.nt.gz', 'rb') as gf:
        images.parse(data=gf.read(), format='nt')

    train, test, valid = main(train, test, valid, user, item, images)

    for graph, filename in ((train, 'ml100k+_train'),
                            (test, 'ml100k+_test'),
                            (valid, 'ml100k+_valid')):
        graph.serialize('./%s.nt' % filename, format='nt')
