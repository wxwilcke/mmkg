#!/usr/bin/env python

import gzip

from rdflib import Graph, URIRef


NS_SCHEMA = "movielens://schema/"
TEST_RATINGS_PER_USER = 10

def ntriple(s, p, o):
    t = ""
    t += '<'+str(s)+'> '
    t += '<'+str(p)+'> '

    if isinstance(o, URIRef):
        t += '<'+str(o)+'> '
    else:
        t += "\"%s\"^^<%s> " % (str(o), str(o.datatype))

    return t + '.'

def main(g):
    user_rating_counter = dict()

    test_ratings = set()
    with gzip.open("./u.data.test.nt.gz", 'wb') as f_test:
        for s, p, o in g.triples((None, URIRef(NS_SCHEMA+"rated"), None)):
            if s not in user_rating_counter.keys():
                user_rating_counter[s] = 0

            user_rating_count = user_rating_counter[s]
            if user_rating_count < TEST_RATINGS_PER_USER:

                f_test.write((ntriple(s, p, o)+'\n').encode())
                for ss, pp, oo in g.triples((o, None, None)):
                    f_test.write((ntriple(ss, pp, oo)+'\n').encode())

                test_ratings.add(o)

            user_rating_counter[s] = user_rating_count + 1

    f_train = gzip.open("./u.data.train.nt.gz", 'wb')
    f_valid = gzip.open("./u.data.valid.nt.gz", 'wb')
    for user, rating_count in user_rating_counter.items():
        rating_count -= TEST_RATINGS_PER_USER
        if rating_count <= 0:
            continue

        i = 0
        valid_n = round(rating_count*.2)
        for s, p, o in g.triples((user, URIRef(NS_SCHEMA+"rated"), None)):
            if o in test_ratings:
                continue

            if i < valid_n:
                f = f_valid
            else:
                f = f_train

            f.write((ntriple(s, p, o)+'\n').encode())
            for ss, pp, oo in g.triples((o, None, None)):
                f.write((ntriple(ss, pp, oo)+'\n').encode())

            i += 1

    f_train.close()
    f_valid.close()

if __name__ == "__main__":
    g = Graph()
    with gzip.open("./u.data.nt.gz", "rb") as gf:
        g.parse(data=gf.read(), format="nt")

    main(g)
