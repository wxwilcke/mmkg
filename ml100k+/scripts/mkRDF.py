#!/usr/bin/env python

from base64 import urlsafe_b64encode
import calendar
from datetime import date, datetime
import gzip
from io import BytesIO
import tarfile
import uuid

import numpy as np
from PIL import Image


NAMESPACE = "https://movielens.org/"
FOLDER_PATH = "movielens-100k/ml-100k/"
_IMG_SIZE = (64, 64)
_IMG_MODE = "RGB"

def ntriple(s, p, o, object_type="uri"):
    t = ""
    t += '<'+NAMESPACE+'resource/'+s.title()+'> '
    t += '<'+NAMESPACE+'schema/'+p+'> '

    if object_type == "uri":
        t += '<'+NAMESPACE+'resource/'+o.title()+'> '
    else:
        t += "\"%s\"^^<http://www.w3.org/2001/XMLSchema#%s> " % (o, object_type)

    return t + '.'

def mkbinary(im):
    b = BytesIO()
    im.save(b, format="JPEG")

    return urlsafe_b64encode(b.getvalue()).decode()

def convert_mode(im):
    if im.mode != _IMG_MODE:
        im = im.convert(_IMG_MODE)

    return im

def downsample(im):
    if im.size != _IMG_SIZE:
        return im.resize(_IMG_SIZE)

    return im

def main():
    genres_dict = dict()
    with tarfile.open("movielens_100k.tar.xz", "r:xz") as tf:
        f_in = tf.extractfile(FOLDER_PATH + "u.genre")
        # genre|ID
        for line in f_in:
            line = line.decode("ISO-8859-1").rstrip()
            if line == '':
                continue

            genre, genre_id = line.split('|')
            genres_dict[int(genre_id)] = genre.title().replace("'", '')

        f_in = tf.extractfile(FOLDER_PATH + "u.data")
        # user id | item id | rating | timestamp
        with gzip.open("u.data"+".nt.gz", 'wb') as f_out:
            for line in f_in:
                line = line.decode("ISO-8859-1").rstrip()
                if line == '':
                    continue

                user_id, item_id, rating, timestamp = line.split('\t')

                bnode = generate_bnode()
                item = "movie_" + item_id
                t = ntriple(bnode, "item", item) + '\n'
                f_out.write(t.encode())

                t = ntriple(bnode, "rating", rating,
                            object_type="positiveInteger") + '\n'
                f_out.write(t.encode())

                tdate = datetime.fromtimestamp(int(timestamp))
                t = ntriple(bnode, "timestamp", tdate.isoformat(),
                            object_type="dateTime") + '\n'
                f_out.write(t.encode())

                user = "user_" + user_id
                t = ntriple(user, "rated", bnode) + '\n'
                f_out.write(t.encode())

        f_in = tf.extractfile(FOLDER_PATH + "u.user")
        # user id | age | gender | occupation | zip code
        with gzip.open("u.user"+".nt.gz", 'wb') as f_out:
            for line in f_in:
                line = line.decode("ISO-8859-1").rstrip()
                if line == '':
                    continue

                user_id, age, gender, occupation, zipcode = line.split('|')

                user = "user_" + user_id
                if age != '':
                    t = ntriple(user, "age", age,
                                object_type="nonNegativeInteger") + '\n'
                    f_out.write(t.encode())

                if gender != '':
                    t = ntriple(user, "gender", gender) + '\n'
                    f_out.write(t.encode())

                if occupation != '':
                    t = ntriple(user, "occupation", occupation.title()) + '\n'
                    f_out.write(t.encode())

                if zipcode != '':
                    if zipcode.isdigit():
                        t = ntriple(user, "zipcode", zipcode,
                                    object_type="nonNegativeInteger") + '\n'
                    else:
                        t = ntriple(user, "zipcode", zipcode,
                                    object_type="string") + '\n'
                    f_out.write(t.encode())

        f_in = tf.extractfile(FOLDER_PATH + "u.item")
        # movie id | movie title | release date | video release date | IMDb URL | genre * 19
        with gzip.open("u.item"+".nt.gz", 'wb') as f_out:
            months_map = {v: k for k,v in enumerate(calendar.month_abbr)}
            for line in f_in:
                line = line.decode("ISO-8859-1").rstrip()
                if line == '':
                    continue

                linesplit = line.split('|')
                movie_id = linesplit[0]
                movie_title = linesplit[1]
                release_date = linesplit[2]
                vid_release_date = linesplit[3]
                imdb_link = linesplit[4]
                genre = linesplit[5:]

                movie = "movie_" + movie_id
                t = ntriple(movie, "title", movie_title,
                            object_type="string") +'\n'
                f_out.write(t.encode())

                if release_date != '':
                    dd, mm, yy = release_date.split('-')
                    rdate = date(int(yy), months_map[mm], int(dd))
                    t = ntriple(movie, "release_date", rdate.isoformat(),
                                object_type="date") +'\n'
                    f_out.write(t.encode())

                if vid_release_date != '':
                    dd, mm, yy = vid_release_date.split('-')
                    rdate = date(int(yy), months_map[mm], int(dd))
                    t = ntriple(movie, "video_release_date", rdate.isoformat(),
                                object_type="date") +'\n'
                    f_out.write(t.encode())

                if imdb_link != '':
                    t = ntriple(movie, "imdb_page", imdb_link,
                                object_type="anyURI") + '\n'
                    f_out.write(t.encode())

                genre_idc = (np.array(genre) == '1').nonzero()[0]
                for genre_idx in genre_idc:
                    t = ntriple(movie, "genre", genres_dict[genre_idx]) + '\n'
                    f_out.write(t.encode())

    with tarfile.open("movielens_images.tar.xz", "r:xz") as tf:
        with gzip.open("b64images.nt.gz", 'wb') as f_out:
            for filename in tf.getmembers():
                if filename.name[-3:] != "jpg":
                    continue

                item_id = filename.name.split('/')[-1][:-4]
                item = "movie_" + item_id

                im = Image.open(tf.extractfile(filename))
                im = convert_mode(im)
                im = downsample(im)

                t = ntriple(item, "depiction", mkbinary(im),
                           object_type="b64string") + '\n'
                f_out.write(t.encode())

def generate_bnode():
    return 'B' + uuid.uuid4().hex

if __name__ == "__main__":
    main()
