#!/usr/bin/env python

from sys import stdin, stdout

from rdf.io import NTriple
from rdf.terms import Literal


# Requires pyRDF
# See https://gitlab.com/wxwilcke/pyRDF

def main(data):
    entities = set()
    predicates = set()
    literals = list()
    datatypes = dict()

    num_facts = 0
    with NTriple(data=data) as g:
        for s, p, o in g.parse():
            num_facts += 1

            entities.add(s)
            predicates.add(p)
            if isinstance(o, Literal):
                literals.append(o)
                if o.datatype is not None:
                    dt = '[DTYPE] ' + str(o.datatype)
                    if dt not in datatypes.keys():
                        datatypes[dt] = 1
                        continue
                    datatypes[dt] = datatypes[dt] + 1
                elif o.language is not None:
                    dt = '[DTYPE] ' + 'http://www.w3.org/2001/XMLSchema#string'
                    if dt not in datatypes.keys():
                        datatypes[dt] = 1
                        continue
                    datatypes[dt] = datatypes[dt] + 1
            else:  # BNode / IRIRef
                entities.add(o)

    unique_literals = set(literals)

    stats = {'Facts': num_facts,
            'Predicates': len(predicates),
            'Entities': len(entities),
            'Literals': len(literals),
            'Literals Unique': len(unique_literals)}
    stats.update(datatypes)

    return stats

if __name__ == "__main__":
    stats = main(stdin.read())
    for k,v in stats.items():
        stdout.write("%s: %d" % (k, v) + '\n')
