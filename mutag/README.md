# MUTAG

`mutag.nt.gz`
- MUTAG benchmark dataset; stripped of target samples

`mutag_train_set.nt.gz`
- training target samples, stripped from dataset, stratified 

`mutag_valid_set.nt.gz`
- validation target samples, stripped from dataset, stratified

`mutag_test_set.nt.gz`
- test target samples, stripped from dataset, stratified

## Statistics

| Stats     | Count  |
|-----------|--------|
| Facts     | 74,567 |
| Relations |     24 |
| Labeled   |    340 |
| Classes   |      2 |
| Entities  | 22,540 |
| Literals  | 11,185 |

| Modality  | Count  |
|-----------|--------|
| Numerical | 11,185 |
| Temporal  |      0 |
| Textual   |      0 |
| Visual    |      0 |
| Spatial   |      0 |
| Other     |      0 |

## Source

Original dataset from:

P. Ristoski, G.K.D. De Vries and H. Paulheim, A collection  of benchmark datasets for systematic evaluations of machine learning on the semantic web, in: International Semantic Web Conference, Springer, 2016, pp. 186–194
