#!/usr/bin/env python

from sys import stdin

from osgeo import ogr
from rdflib import Namespace, Literal, Graph, URIRef
from rdflib.namespace import XSD


_OGC_NAMESPACE = Namespace(URIRef("http://www.opengis.net/ont/geosparql#"))

def GMLtoWKT(geom):
    geom = ogr.CreateGeometryFromGML(geom)
    if geom is None:
        return None

    return geom.ExportToWkt()

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def main(g):
    for s, p, o in g.triples((None, None, None)):
        if p == URIRef('http://data.bgs.ac.uk/ref/Spatial/hasFootprint') or\
           p == URIRef('http://data.bgs.ac.uk/ref/Spatial/hasBoundingBox') or\
           p == URIRef('http://data.bgs.ac.uk/ref/Spatial/hasPointLocation') or\
           p == URIRef('http://purl.org/dc/terms/spatial') :
            geom = GMLtoWKT(o)
            if geom is None:
                continue
            g.remove((s, p, o))
            g.add((s, p, Literal(geom, datatype=_OGC_NAMESPACE.wktLiteral)))
        if o.startswith("http://data.bgs.ac.uk/id/EarthMaterialClass/RockName/"):
            # fix bad URI
            o = str(o).replace("^", '%5E')  # urlencode '^'
            g.remove((s, p, o))
            g.add((s, p, URIRef(o)))


if __name__ == "__main__":
    g = Graph()
    g.parse(data=stdin.read(), format='nt')

    main(g)
    g.serialize('./out.nt', format='nt')
