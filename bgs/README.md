# BGS+

`bgs+.nt.gz`
- BGS benchmark dataset enriched with datatype declarations; stripped of target samples

`bgs+_train_set.nt.gz`
- training target samples, stripped from dataset, stratified 

`bgs+_valid_set.nt.gz`
- validation target samples, stripped from dataset, stratified

`bgs+_test_set.nt.gz`
- test target samples, stripped from dataset, stratified

`scripts/`
- scripts to convert BGS to BGS+

## Statistics

| Stats     | Count   |
|-----------|---------|
| Facts     | 916,345 |
| Relations |     104 |
| Labeled   |     146 |
| Classes   |       2 |
| Entities  | 103,055 |
| Literals  | 386,254 |

| Modality  | Count   |
|-----------|---------|
| Numerical |  12,332 |
| Temporal  |      13 |
| Textual   | 262,585 |
| Visual    |       0 |
| Spatial   |  73,870 |
| Other     |  20,098 |

## Source

Original dataset from:

P. Ristoski, G.K.D. De Vries and H. Paulheim, A collection  of benchmark datasets for systematic evaluations of machine learning on the semantic web, in: International Semantic Web Conference, Springer, 2016, pp. 186–194
