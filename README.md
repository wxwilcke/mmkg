# Benchmark Datasets for (Multimodal) Learning on Knowledge Graphs

Benchmark datasets for (multimodal) machine learning in RDF format. Many datasets have been enriched with additional
modalities, and all datasets have proper datatype declarations to enable machine learning models to automatically infer
the type of encoded information.

## Datasets

Classification

- AIFB+
- MUTAG
- BGS+
- AM+
- DMG
- SYNTH

Link Prediction

- FB15K
- FB15K-237
- WN18RR
- ML100K+
- YAGO3-10+

