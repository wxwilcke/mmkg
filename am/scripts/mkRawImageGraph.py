#!/usr/bin/env python

from base64 import urlsafe_b64encode
from io import BytesIO
from requests import session
from shutil import copyfileobj
from sys import stdin, stdout, stderr
from time import sleep

from rdflib import BNode, Literal, Graph
from rdflib.namespace import Namespace, RDF, XSD, DCTERMS


EUNS = Namespace("http://www.europeana.eu/schemas/edm/")
IMG_WIDTH = 200
IMG_HEIGHT = 200
REQUEST_TIMEOUT = 0.2

def ntriple(t):
    triple = ""
    for term in t:
        if type(term) is Literal:
            triple += "\"{}\"".format(str(term))

            if term.datatype is not None:
                triple += "^^<{}>".format(str(term.datatype))
            elif term.language is not None:
                triple += "@{}".format(str(term.language))

            triple += ' '
        elif type(term) is BNode:
            triple += "_:{} ".format(str(term))
        else:  # URIRef
            triple += "<{}> ".format(str(term))

    return triple + '.'

def retrieve_resource(session, href):
    req = session.get(href, stream=True)

    t = 2
    while req.status_code == 429 and t <= 32:
        sleep(t) # Give it time to recuperate
        req = session.get(href, stream=True)

        t *= 2

    if not req.status_code == 200:
        return None

    return req

def mkbinary(raw_img):
    raw_img.decode_content = True
    bytesIO = BytesIO()
    copyfileobj(raw_img, bytesIO)

    return urlsafe_b64encode(bytesIO.getvalue()).decode()

def main(g):
    ses = session()
    for href_img in g.subjects(RDF.type, EUNS.WebResource):
        href_img_scaled = str(href_img) + "&width=%d&height=%d" % (IMG_WIDTH, IMG_HEIGHT)

        img = retrieve_resource(ses, href_img_scaled)
        if img is None:
            stderr.write("Failed request on image %s\n" % href_img_scaled)
            sleep(REQUEST_TIMEOUT)

            continue

        # write triple
        b64string = mkbinary(img.raw)
        t = ntriple((href_img, DCTERMS.hasFormat, Literal(b64string,
                                                  datatype=XSD.b64string)))
        stdout.write(t+'\n')

        # Don't flood the server
        sleep(REQUEST_TIMEOUT)

if __name__ == "__main__":
    g = Graph()
    g.parse(data=stdin.read(), format='nt')

    main(g)
