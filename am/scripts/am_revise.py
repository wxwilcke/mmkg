#!/usr/bin/env python

from sys import stdin

from rdflib import Namespace, Literal, Graph
from rdflib.namespace import XSD


AMNS = Namespace("http://purl.org/collections/nl/am/")

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def main(g):
    for s, p, o in g.triples((None, None, None)):
        if type(o) is not Literal:
            continue

        if p == AMNS.birthDateStart or\
           p == AMNS.birthDateEnd or\
           p == AMNS.deathDateStart or\
           p == AMNS.deathDateEnd or\
           p == AMNS.currentLocationDateStart or\
           p == AMNS.currentLocationDateEnd or\
           p == AMNS.exhibitionDateStart or\
           p == AMNS.exhibitionDateEnd or\
           p == AMNS.alternativeNumberDate or\
           p == AMNS.productionDateStart or\
           p == AMNS.productionDateEnd or\
           p == AMNS.AHMTextsDate or\
           p == AMNS.AHMTextsPubl or\
           p == AMNS.acquisitionDate or\
           p == AMNS.reproductionDate:
            g.remove((s, p, o))
            o = str(o)
            if len(o) == 4:
                o += "-01-01"
            elif len(o) == 6:
                o += "-01"
            g.add((s, p, Literal(o, datatype=XSD.date)))
            continue
        if p == AMNS.documentationSortyear:
            g.remove((s, p, o))
            o = str(o)
            g.add((s, p, Literal(o, datatype=XSD.gYear)))
            continue
        if p == AMNS.reproductionReferenceLref or\
           p == AMNS.documentationTitleLref or\
           p == AMNS.priref or\
           p == AMNS.exhibitionLref or\
           p == AMNS.currentLocationLref or\
           p == AMNS.AHMTextsAuthorLref:
            if not is_number(o.value):
                continue
            g.remove((s, p, o))
            g.add((s, p, Literal(o.value, datatype=XSD.positiveInteger)))
            continue
        if p == AMNS.dimensionValue:
            if not is_number(o.value):
                continue
            g.remove((s, p, o))
            g.add((s, p, Literal(o.value, datatype=XSD.decimal)))
            continue
        if p == AMNS.selected:
            o = str(o).lower()
            g.remove((s, p, o))
            g.add((s, p, Literal(o, datatype=XSD.boolean)))
            continue

        g.remove((s, p, o))
        o = str(o)
        g.add((s, p, Literal(o, datatype=XSD.string)))

if __name__ == "__main__":
    g = Graph()
    g.parse(data=stdin.read(), format='nt')

    main(g)
    g.serialize('./out.nt', format='nt')
