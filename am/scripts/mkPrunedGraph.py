#!/usr/bin/env python

import gzip
from sys import stdin, stdout

from rdflib import BNode, Literal, Graph


TRAIN_SET = "./am_train_set.nt.gz"
TEST_SET = "./am_test_set.nt.gz"
VALID_SET = "./am_valid_set.nt.gz"

def ntriple(t):
    triple = ""
    for term in t:
        if type(term) is Literal:
            term_str = str(term)
            term_str = term_str.replace('\n', '%0d').replace('\t', '%09')
            term_str = term_str.replace('\"', '\\"').replace('\'', "\\'")
            triple += "\"{}\"".format(term_str)

            if term.datatype is not None:
                triple += "^^<{}>".format(str(term.datatype))
            elif term.language is not None:
                triple += "@{}".format(str(term.language))

            triple += ' '
        elif type(term) is BNode:
            triple += "_:{} ".format(str(term))
        else:  # URIRef
            triple += "<{}> ".format(str(term))

    return triple + '.'

def prune(g, entities, depth=2):
    triples = set()
    if depth <= 0 or len(entities) <= 0:
        return triples

    neighbours = set()
    # forward prune
    for triple, entity in sample_forwards(g, entities):
        triples.add(triple)

        if depth > 0 and entity is not None:
            neighbours.add(entity)

    # backward prune
    for triple, entity in sample_backwards(g, entities):
        triples.add(triple)

        if depth > 0:
            neighbours.add(entity)

    triples |= prune(g, neighbours, depth-1)

    return triples

def sample_forwards(g, entities):
    for e in entities:
        for s,p,o in g.triples((e, None, None)):
            if not isinstance(o, Literal):
                yield ((s,p,o), o)
            else:
                yield ((s,p,o), None)

def sample_backwards(g, entities):
    for e in entities:
        for s,p,o in g.triples((None, None, e)):
            yield ((s,p,o), s)

def get_samples(train_set, test_set, valid_set):
    h = Graph()
    for dataset in (train_set, test_set, valid_set):
        with gzip.open(dataset, 'rb') as gd:
            h.parse(data=gd.read(), format='nt')

    return set(h.subjects())

if __name__ == "__main__":
    g = Graph()

    samples = get_samples(TRAIN_SET, TEST_SET, VALID_SET)
    g.parse(data=stdin.read(), format='nt')
    for triple in prune(g, samples, depth=2):
        stdout.write(ntriple(triple)+'\n')
