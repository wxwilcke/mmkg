#!/usr/bin/env python

from requests import session
from sys import stdin, stdout, stderr
from time import sleep

from lxml import html
from rdflib import BNode, Literal, Graph, URIRef
from rdflib.namespace import Namespace, RDF


HTML_AM_BASEURI = "http://am.adlibhosting.com/amonline/advanced/Details/collect/"
HTML_IMG_CLASS = "ais-detail-image-viewer-link ais-detail-image-viewer-link-script"
HTML_IMG_XPATH = "//a[@class='{}']".format(HTML_IMG_CLASS)
AMNS = Namespace("http://purl.org/collections/nl/am/")
EUNS = Namespace("http://www.europeana.eu/schemas/edm/")
REQUEST_TIMEOUT = 0.2

def ntriple(t):
    triple = ""
    for term in t:
        if type(term) is Literal:
            triple += "\"{}\"".format(str(term))

            if term.datatype is not None:
                triple += "^^<{}>".format(str(term.datatype))
            elif term.language is not None:
                triple += "@{}".format(str(term.language))

            triple += ' '
        elif type(term) is BNode:
            triple += "_:{} ".format(str(term))
        else:  # URIRef
            triple += "<{}> ".format(str(term))

    return triple + '.'

def retrieve_resource(session, href):
    req = session.get(href, stream=True)

    t = 2
    while req.status_code == 429 and t <= 64:
        sleep(t) # Give it time to recuperate
        req = session.get(href, stream=True)

        t *= 2

    if not req.status_code == 200:
        return None

    return req

def main(g):
    ses = session()
    proxy_ids = set(g.objects(None, AMNS.priref))
    for proxy_id in proxy_ids:
        href_proxy = HTML_AM_BASEURI + str(proxy_id)
        page = retrieve_resource(ses, href_proxy)
        if page is None:
            stderr.write("Failed reques on proxy %s\n" % proxy_id)
            sleep(REQUEST_TIMEOUT)

            continue

        tree = html.fromstring(page.content)
        elem_list = tree.xpath(HTML_IMG_XPATH)
        for elem in elem_list:
            if "href" not in elem.attrib.keys():
                continue

            href_img = URIRef(str(elem.attrib['href']).replace('\\', '%5C').replace(' ', '%20'))

            # write triples
            aggregation_uri = URIRef(AMNS.aggregation- + str(proxy_id))
            stdout.write(ntriple((href_img, RDF.type, EUNS.WebResource))+'\n')
            stdout.write(ntriple((aggregation_uri, EUNS.object, href_img))+'\n')

if __name__ == "__main__":
    g = Graph()
    g.parse(data=stdin.read(), format='nt')

    main(g)
