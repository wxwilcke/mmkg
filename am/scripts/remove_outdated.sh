# filter outdated triples from input
# zcat ../<file>.nt.gz | ./remove_outdated.sh | gzip > <file_filtered>.nt.gz

# make temporary directory
tempdir=$(mktemp -d "${TMPDIR:-/tmp/}$(basename $0).XXXXXXXXXXXX")
patch="am_updates.nt.gz"

# read input
cat > $tempdir/input

# get lines to be removed - part 1
zgrep -F "aggregation" $patch | sed "s/\(<.*> <.*> \)<.*> \./\1/" > $tempdir/aggregators
cat $tempdir/input | grep -Ff $tempdir/aggregators > $tempdir/matches

# get lines to be removed - part 2
predicateObject="<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.europeana.eu/schemas/edm/WebResource> ."
sed "s|<.*> <.*> \(<.*>\) \.|\1 $predicateObject|" $tempdir/matches > $tempdir/matchesB

# remove lines
cat $tempdir/matchesB >> $tempdir/matches
cat $tempdir/input | grep -vwFf $tempdir/matches

# cleanup
rm -rf $tempdir
