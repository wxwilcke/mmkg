# AM2D+

`am.nt.gz`
- AM benchmark dataset enriched with datatype declarations and b64-encoded images; stripped of target samples

`am2d+.nt.gz`
- AM benchmark dataset enriched with datatype declarations and b64-encoded images; stripped of target samples; pruned from am.nt.gz to only include up to depth two from target entities

`am_train_set.nt.gz`
- training target samples, stripped from dataset, stratified 

`am_valid_set.nt.gz`
- validation target samples, stripped from dataset, stratified

`am_test_set.nt.gz`
- test target samples, stripped from dataset, stratified

`scripts/`
- scripts to convert AM to AM+

## Statistics

| Stats     | Count   |
|-----------|---------|
| Facts     | 639,190 |
| Relations | 123     |
| Labeled   | 1000    |
| Classes   | 11      |
| Entities  | 146,609 |
| Literals  | 53,614  |

| Modality  | Count  |
|-----------|--------|
| Numerical | 11,113 |
| Temporal  | 14,798 |
| Textual   | 26,870 |
| Visual    |    812 |
| Spatial   |      0 |
| Other     |      0 |

## Source

Original dataset from:

P. Ristoski, G.K.D. De Vries and H. Paulheim, A collection  of benchmark datasets for systematic evaluations of machine learning on the semantic web, in: International Semantic Web Conference, Springer, 2016, pp. 186–194
