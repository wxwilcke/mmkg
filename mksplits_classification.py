#!/usr/bin/env python

from math import ceil
import gzip
import sys

from rdf.io import NTriple
from rdf.graph import Statement


def divide_members(samples_map, test_ratio, valid_ratio = 0, meta_ratio = 0):
    sample_assignments = dict()
    for predicate, num_samples in samples_map.items():
        test_portion = ceil(num_samples * test_ratio)
        valid_portion = ceil(num_samples * valid_ratio)
        meta_portion = ceil(num_samples * meta_ratio)
        train_portion = num_samples - (test_portion + valid_portion + meta_portion)

        if train_portion <= 0:
            train_portion = num_samples
            test_portion = 0
            valid_portion = 0
            meta_portion = 0

        sample_assignments[predicate] = {'train': train_portion,
                                         'test': test_portion,
                                         'valid': valid_portion,
                                         'meta': meta_portion}

    return sample_assignments

def generate_splits(g, h, split_sizes, stratified):
    if len(split_sizes) < 1:
        print_usage_and_exit()

    num_samples = 0
    samples_map = dict()
    for _, _, o in g.parse():
        if o not in samples_map.keys():
            samples_map[o] = 0
        samples_map[o] = samples_map[o] + 1

        num_samples += 1

    num_samples_test = split_sizes[0]
    num_samples_valid = 0 if len(split_sizes) < 2 else split_sizes[1]
    num_samples_meta = 0 if len(split_sizes) < 3 else split_sizes[2]

    test_ratio = num_samples_test / num_samples
    valid_ratio = num_samples_valid / num_samples
    meta_ratio = num_samples_meta / num_samples

    counts = {'train': 0, 'test': 0, 'valid': 0, 'meta': 0}
    if not stratified:
        sample_assignments = divide_members({'*': num_samples}, test_ratio, valid_ratio, meta_ratio)
        for s, p, o in g.parse():
            for split in ('train', 'test', 'valid', 'meta'):
                remain = sample_assignments['*'][split]
                if remain > 0:
                    h[split].write(Statement(s, p, o))
                    sample_assignments['*'][split] = remain - 1

                    counts[split] = counts[split] + 1

                    break

    else:
        sample_assignments = divide_members(samples_map, test_ratio, valid_ratio, meta_ratio)
        for s, p, o in g.parse():
            for split in ('train', 'test', 'valid', 'meta'):
                remain = sample_assignments[o][split]
                if remain > 0:
                    h[split].write(Statement(s, p, o))
                    sample_assignments[o][split] = remain - 1

                    counts[split] = counts[split] + 1

                    break

    print('split distribution:')
    for split, count in counts.items():
        print('- %d %s' % (count, split))

def print_usage_and_exit():
    print("USAGE: cat <samples.nt> | python ./mksplits_classification.py [<test_set_size> <valid_set_size> <meta_set_size>]")
    sys.exit(1)

if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) < 1 or len(args) > 3:
        print_usage_and_exit()

    g = NTriple(data=sys.stdin.read(), mode = 'r')

    g_train = NTriple(path='train.nt', mode = 'w')
    g_test = NTriple(path='test.nt', mode = 'w')
    g_valid = NTriple(path='valid.nt', mode = 'w') if len(args) >= 2 else None
    g_meta = NTriple(path='meta.nt', mode = 'w') if len(args) >= 3 else None

    generate_splits(g, {'train': g_train, 'test': g_test, 'valid': g_valid, 'meta': g_meta},
                    [int(v) for v in args],
                    stratified=True)

