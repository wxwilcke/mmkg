#!/usr/bin/env python

import gzip
import tarfile


NAMESPACE = "http://wordnet-rdf.princeton.edu/id/"
NAMESPACE_SCHEMA = "http://wordnet-rdf.princeton.edu/ontology#"

def ntriple(s, p, o):
    t = ""
    for e in (s, p, o):
        if e.startswith('_'):
            t += '<' + NAMESPACE_SCHEMA + e + '> '
        else:
            t += '<' + NAMESPACE + e + '> '

    return t + ".\n"

def main():
    with tarfile.open("wn18rr.tgz", "r:gz") as tf:
        for filename in ("train", "valid", "test"):
            f_in = tf.extractfile("wn18rr/" + filename+".txt")

            with gzip.open("wn18rr_"+filename+".nt.gz", "wb") as f_out:
                for line in f_in:
                    line = line.decode().rstrip()

                    t = ntriple(*line.split('\t'))
                    f_out.write(t.encode())

if __name__ == "__main__":
    main()
