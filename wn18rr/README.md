# WN18RR

WordNet dataset

`wn18rr-train.nt.gz`
- training split; RDF version of original 

`wn18rr-test.nt.gz`
- test split; RDF version of original 

`wn18rr-valid.nt.gz`
- validation split; RDF version of original 

`scripts/`
- scripts to convert WN18RR to RDF

## Statistics

| Stats     | Count  |
|-----------|--------|
| Facts     | 93,003 |
| Relations | 11     |
| Entities  | 40,943 |
| Literals  | 0      |

| Modality  | Count |   |
|-----------|-------|---|
| Numerical | 0     |   |
| Temporal  | 0     |   |
| Textual   | 0     |   |
| Visual    | 0     |   |
| Spatial   | 0     |   |
| Other     | 0     |   |

## Source

Original dataset from:

Bordes, Antoine, et al. "Translating embeddings for modeling multi-relational data." Neural Information Processing Systems (NIPS). 2013.
