# SYNTH

Synthetic dataset for testing purposes and unit tests.

Generated with https://gitlab.com/wxwilcke/graphsynth

`synth_context.nt.gz`
- SYNTH synthetic benchmark dataset for multimodal learning on knowledge graphs; stripped of target samples

`synth_train.nt.gz`
- training target samples, stripped from dataset, stratified 

`synth_valid.nt.gz`
- validation target samples, stripped from dataset, stratified

`synth_test.nt.gz`
- test target samples, stripped from dataset, stratified

## Statistics

| Stats     | Count   |
|-----------|---------|
| Facts     | 181,641 |
| Relations | 42      |
| Labeled   | 16,386  |
| Classes   | 2       |
| Entities  | 16,386  |
| Literals  | 132,489 |

| Modality  | Count  |
|-----------|--------|
| Numerical | 29,565 |
| Temporal  | 44,207 |
| Textual   | 29,540 |
| Visual    | 14,780 |
| Spatial   | 14,705 |
| Other     | 0      |
